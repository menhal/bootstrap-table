/**
 * @author: Dennis Hernández
 * @webSite: http://djhvscf.github.io/Blog
 * @version: v2.2.0
 */

const UtilsFilterControl = {
  createControls(that, column){
    const $th = UtilsFilterControl.findField(that.$header, column)
    const $field = $th.data('field');
    const createControl = column.filter;

    const controlSelector = createControl(data => UtilsFilterControl.onFilter(that, $field, data, $th, controlSelector));
    const control = $(controlSelector)
    // $(control).addClass("filter-control-controls");

    const container = $("<div class='filter-control'>" +
        "<button type='button' class='filter-toggle'><i class=\"fa fa-search\"></i></button>" +
        "<div class='filter-control-controls'><div class='filter-control-controls-wrapper'></div>" +
        "<button class='btn btn-custom btn-sm'><i class=\"fa fa-search\"></i>搜索</button>" +
        "<button class='btn btn-light btn-sm' style='margin-left: 10px'>重置</button></div> </div>")

    container.on("click", ".filter-toggle", () => UtilsFilterControl.onShow($th, controlSelector));

    container.on("click", ".btn-custom", () => {
      $("#search").click();
      UtilsFilterControl.onClose(that, $th, controlSelector);
    });

    container.on("click", ".btn-light", () => {
      $("input, select", control).val("");
      $("#search").click();
      UtilsFilterControl.onClose(that, $th, controlSelector);
    });

    $th.off('keypress')

    $(document).on("click", function (evt) {
      if($(evt.target).closest(UtilsFilterControl.findField(that.$header, column)).length === 0 && $(evt.target).closest(control).length === 0 && $(evt.target).closest(".layui-layer").length === 0) {
        UtilsFilterControl.onClose(that, $th, controlSelector);
      }
    });

    // container.append(control);

    $th.find('.fht-cell').append(container)
    $th.addClass("filter-control-wrapper");
  },

  onFilter(that, $field, data, $th, controlSelector){
    Object.assign(that.filterColumnsPartial, {[$field]: data})
    that.onSearch({}, false)
    UtilsFilterControl.onClose(that, $th);
  },

  onShow($th, controlSelector){
    $(".filter-control-controls", $th);
    $(".filter-control", $th).toggleClass("filter-control-active")

    let formControl = $(controlSelector).show()

    let width = formControl.outerWidth();
    let height = formControl.outerHeight();

    $(".filter-control-controls-wrapper", $th).css({width, height});
    let offset2 = $(".filter-control-controls-wrapper", $th).offset();

    formControl.css({left: offset2.left, top: offset2.top})

    // setTimeout(function () {
    //   $("body").css("overflow", "hidden");
    // })
  },

  onClose(that, $th, controlSelector){
    const $field = $th.data('field');
    const data = that.filterColumnsPartial[$field];
    $(".filter-control", $th).removeClass("filter-control-active")
    $(".filter-toggle", $th).toggleClass("filter-toggle-active", !!data)
    $(controlSelector).hide();
    $("body").css("overflow", "auto");
  },

  findField(header, column){
    let field;
    $.each(header.children().children(), (i, th) => {
      const $th = $(th)
      if ($th.data('field') === column.field) {
        field = $th
      }
    });
    return field;
  },
}

$.BootstrapTable = class extends $.BootstrapTable {

  init () {
    this.filterColumnsPartial = this.filterColumnsPartial || {};
    super.init()
  }

  initHeader () {
    super.initHeader()
    const that = this;

    $.each(that.columns, (i, column) => {
      if (!column.visible) {
        return
      }

      if (column.filter) {
        UtilsFilterControl.createControls(that, column);
      }
    })
  }
}
